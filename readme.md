﻿﻿﻿Strict DLP Chinese (SDC)
=====

<p align="center">
<a href="https://gitlab.com/chengr28/specialdlp/blob/master/license"><img src="https://img.shields.io/gitlab/license/chengr28/specialdlp"></a> <a href="https://gitlab.com/chengr28/specialdlp/-/releases"><img src="https://img.shields.io/gitlab/v/release/chengr28/specialdlp"></a>
</p>

Strict DLP Chinese (SDC) is a set of strict DLP (Dynamic Leech Protection) DLLs based on the eMule Xtreme Mod's official version. SDC variants put easyMule v2, easyMule v1 and/or eMule VeryCD Mod into "Soft Ban" or "Hard Ban" list because of GPL violation, private network, community leeching, and other behaviors.

<p align="center">
<a href="readme.zh-hans.md">简体中文介绍</a> | <a href="readme.zh-hant.md">繁體中文介紹</a>
</p>

* [Feature table for SDC variants](https://gitlab.com/chengr28/specialdlp/-/blob/master/specialdlp/document/readme.en.md)
* [Download a single SDC variant](https://gitlab.com/chengr28/specialdlp/-/tree/binary)
* [Releases page: Download all SDC variants in a signle package](https://gitlab.com/chengr28/specialdlp/-/releases)
* [FAQs and other tips](https://gitlab.com/chengr28/specialdlp/-/blob/master/specialdlp/document/readme.en.txt)
* [Changelog](https://gitlab.com/chengr28/specialdlp/-/blob/master/specialdlp/document/changelog.en.txt)
* [Checksum](https://gitlab.com/chengr28/specialdlp/-/blob/master/specialdlp/document/checksum.md)

## External Links

* [Project in GitHub](https://github.com/chengr28/specialdlp)
* [Project in Sourceforge](https://sourceforge.net/projects/specialdlp)
* [SDC in eMuleFans 电骡爱好者](https://emulefans.com/news/plugin/dlp/sdc)
